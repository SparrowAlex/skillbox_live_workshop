describe('Test suite 1', () => {
    it('test_case_1', async () => {
        const expectedProfessionCount = 162;

        await browser.url('http://skillbox.ru');
        const blockProf = await $('.//h3[contains(text(), "Профессии")]/following-sibling::a/span');
        const text = await blockProf.getText();
        expect(text).toContain(expectedProfessionCount.toString());
        await blockProf.click();

        // const puppeteer = await browser.getPuppeteer();
        // const page = (await puppeteer.pages())[0];
        // await page.waitForResponse(async (response) => {
        //     const url = await response.url();
        //     const body = await response.json();
        //     return url === 'https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession/?page=1&limit=10&type=profession'
        //            && body.meta.total === expectedProfessionCount;
        // });
        const buttonProf = await $('.courses-block > .courses-block__load');
        expect(await buttonProf.getText()).toContain(`Ещё 10 профессий из ${expectedProfessionCount - 10}`);
    });

    it.only('test_case_2', async () => {
        const expectedProfessionCount = 162;
        const expectedShowArtiles = 20;

        await browser.url('http://skillbox.ru');
        const blockProf = await $('.//h3[contains(text(), "Профессии")]/following-sibling::a');
        await blockProf.click();

        const buttonProf = await $('.courses-block > .courses-block__load');
        await buttonProf.click();

        // const puppeteer = await browser.getPuppeteer();
        // const page = (await puppeteer.pages())[0];
        // await page.waitForRequest(async (request) => {
        //     return (await request.url()).includes('?page=2&type=profession');
        // });

        // await browser.debug()
        await browser.waitUntil(async () => {
            const buttonProf = await $('.courses-block > .courses-block__load');
            return (await buttonProf.getText()).includes(`Ещё 10 профессий из ${expectedProfessionCount - expectedShowArtiles}`);
        });
        const articles = await $$('article.card');
        expect(articles.length).toEqual(expectedShowArtiles);
    });

    afterEach(async () => {
        await browser.saveScreenshot('failure.png');
    });
});
